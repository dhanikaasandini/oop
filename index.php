<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$object = new Animal("sheep");

echo "Nama Binatang : ".$object->name."<br>";
echo "Jumlah Kaki : ".$object->legs."<br>";
echo "Berdarah Dingin: ".$object->cold_blooded."<br><br>";

$object2 = new Frog("buduk");

echo "Nama Binatang : " .$object2->name. "<br>";
echo "Jumlah Kaki : ".$object2->legs. "<br>";
echo "Berdarah Dingin: ".$object2->cold_blooded."<br>";
echo "adalah  Kodok Lompat " .$object2->jump() .  "<br><br>" ;

$object3 = new Ape ("kera sakti");

echo "Nama Binatang : " .$object3->name. "<br>";
echo "Jumlah Kaki : ".$object3->legs. "<br>";
echo "Berdarah Dingin: ".$object3->cold_blooded."<br>";
echo "adalah  Suara Sungkokong  " .$object3->yell() ; 