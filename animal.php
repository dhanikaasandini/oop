<?php

class Animal
{
    public $name;
    public $legs = 2;
    public $cold_blooded = "False";

    public function __construct($string)
    {
        $this->name = $string;
    }
}


// $sheep = new Animal("shaun");

// echo $sheep->name; // "shaun"
// echo $sheep->legs; // 2
// echo $sheep->cold_blooded // false

